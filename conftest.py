import pytest

from django.conf import settings
from django.db.models import QuerySet

from apps.users.tests.factories import UserFactory
from apps.core.services.currency import currency_sync_with_exchangeratesapi
from apps.core.models import CurrencyRate


@pytest.fixture()
def user(base_currency_rate: CurrencyRate) -> settings.AUTH_USER_MODEL:
    return UserFactory(currency_id=base_currency_rate.id)


@pytest.fixture()
def currency_rates() -> QuerySet:
    currency_sync_with_exchangeratesapi()
    return CurrencyRate.objects.all()


@pytest.fixture()
def base_currency_rate(currency_rates: QuerySet) -> CurrencyRate:
    return CurrencyRate.objects.get(currency=settings.BASE_CURRENCY)
