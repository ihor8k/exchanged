FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE config.settings

WORKDIR /usr/src/app

RUN set -ex \
    && addgroup -g 82 -S www-data \
    && adduser -u 82 -D -S -G www-data www-data \
    && apk add --upgrade --no-cache --virtual .build-deps \
        openssl-dev \
    && apk add --upgrade --virtual \
        build-base \
        gcc \
        libc-dev \
        postgresql-dev \
        python3-dev \
    && apk --no-cache add --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing watchman

COPY . /usr/src/app

RUN set -ex \
    && pip install --upgrade pip \
    && pip install -r /usr/src/app/requirements.txt --no-cache-dir \
    && apk del --no-cache .build-deps \
    && chown -R www-data:www-data /usr/src/app
