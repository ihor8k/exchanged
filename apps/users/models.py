from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.core.types import TypesTransaction
from apps.users.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    balance = models.DecimalField(_('Balance'), max_digits=19, decimal_places=10, blank=True)
    currency = models.ForeignKey('core.CurrencyRate', verbose_name=_('Currency'), on_delete=models.CASCADE, related_name='users', editable=False)
    date_created = models.DateTimeField(_('Date created'), auto_now_add=True)
    date_modified = models.DateTimeField(_('Date modified'), auto_now=True)
    email = models.EmailField(_('E-mail'), db_index=True, unique=True)
    is_active = models.BooleanField(_('Is active'), blank=True, default=True)
    is_staff = models.BooleanField(_('Is staff'), blank=True, default=False)
    name = models.CharField(_('Name'), max_length=128)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def delete(self, *args, **kwargs):
        self.is_active = False
        self.save(update_fields=['is_active'])
        return self


class UserBalanceHistory(models.Model):
    amount = models.DecimalField(_('Amount'), max_digits=19, decimal_places=10, blank=True)
    date_created = models.DateTimeField(_('Date created'), auto_now_add=True)
    type_transaction = models.CharField(_('Type of transaction'), max_length=1, choices=[(tag.value, tag.value) for tag in TypesTransaction])
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='balance_history')
