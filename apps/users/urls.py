from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.users import views


app_name = 'users'

router = DefaultRouter(trailing_slash=False)
router.register('users/balance/history', views.UserBalanceHistoryViewSet, 'users-balance-history')
router.register('users', views.UserViewSet, 'users')

urlpatterns = [
    path('', include(router.urls)),
]
