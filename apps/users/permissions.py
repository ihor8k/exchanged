from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import BasePermission


class HasModelPermissions(BasePermission):
    perms_map = {
        'GET': ('%(app_label)s.view_%(model_name)s',),
        'POST': ('%(app_label)s.add_%(model_name)s',),
        'PATCH': ('%(app_label)s.change_%(model_name)s',),
        'DELETE': ('%(app_label)s.delete_%(model_name)s',),
    }

    def get_required_permissions(self, method, model_cls):
        kwargs = {'app_label': model_cls._meta.app_label, 'model_name': model_cls._meta.model_name}

        if method not in self.perms_map:
            raise MethodNotAllowed(method)

        return [perm % kwargs for perm in self.perms_map[method]]

    def _get_model(self, view):
        assert hasattr(view, 'permission_model') is not None, f'Cannot apply {self.__class__.__name__} on a view that does not set `permission_model`.'

        return view.permission_model

    def has_permission(self, request, view):
        if not request.user or not request.user.is_authenticated:
            return False

        model = self._get_model(view)
        perms = self.get_required_permissions(request.method, model)

        return request.user.has_perms(perms)


class ActionViewSetPermission(BasePermission):
    action_map = {
        'GET': ('list', 'retrieve'),
        'POST': ('create',),
        'PATCH': ('partial_update',),
        'DELETE': ('destroy',)
    }

    def _get_action_permissions(self, view):
        assert hasattr(view, 'action_permissions') is not None, f'Cannot apply {self.__class__.__name__} on a view that does not set `action_permissions`.'
        return view.action_permissions

    def get_required_permissions(self, method, action_permissions):
        if method not in self.action_map:
            raise MethodNotAllowed(method)
        return list(set(action_permissions[action] for action in self.action_map[method] if action in action_permissions))

    def has_permission(self, request, view):
        if not request.user or not request.user.is_authenticated:
            return False

        action_permissions = self._get_action_permissions(view)
        perms = self.get_required_permissions(request.method, action_permissions)

        return request.user.has_perms(perms)
