from typing import Any, Sequence
from django.contrib.auth import get_user_model
from factory import DjangoModelFactory, Faker, post_generation


class UserFactory(DjangoModelFactory):
    email = Faker('email')
    name = Faker('name')
    balance = 888.80

    class Meta:
        model = get_user_model()
        django_get_or_create = ['email']

    @classmethod
    def bulk_create(cls, count_create: int = 5):
        return [cls() for i in range(count_create)]

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs) -> None:
        password = Faker(
            'password',
            length=12,
            special_chars=True,
            digits=True,
            upper_case=True,
            lower_case=True,
        ).generate(extra_kwargs={})
        self.not_hash_password = password
        self.set_password(password)


class SuperUserFactory(UserFactory):
    is_staff = True
    is_superuser = True
