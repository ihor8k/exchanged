import pytest

from django.conf import settings
from django.contrib.auth import get_user_model
from apps.core.models import CurrencyRate
from apps.users import services


User = get_user_model()


@pytest.mark.django_db
def tests_create_user(base_currency_rate: CurrencyRate) -> None:
    data = {'email': 'user@exchanged.com',
            'name': 'Star Wars',
            'balance': 8888,
            'password2': '654321qw',
            'currency_id': base_currency_rate.id}
    instance = services.create_user(data)
    assert User.objects.filter(id=instance.id).exists()
    assert User.objects.get(id=instance.id).check_password(data['password2'])
    assert User.objects.get(id=instance.id).currency_id == base_currency_rate.id
    assert User.objects.get(id=instance.id).email == data['email']
    assert User.objects.get(id=instance.id).name == data['name']
    assert User.objects.get(id=instance.id).balance == data['balance']
    assert User.objects.get(id=instance.id).is_active


@pytest.mark.django_db
def tests_update_user(user: settings.AUTH_USER_MODEL) -> None:
    data = {'name': 'c3po'}
    services.update_user(user, data)
    assert User.objects.get(id=user.id).name == data['name']

    data = {'email': 'c3po@exchanged.com'}
    services.update_user(user, data)
    assert User.objects.get(id=user.id).email == data['email']

    data = {'password2': 'c3p$5o8Y'}
    services.update_user(user, data)
    assert User.objects.get(id=user.id).check_password(data['password2'])
