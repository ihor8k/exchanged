from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from apps.core.models import CurrencyRate
from apps.users.tests.factories import SuperUserFactory
from apps.core.services.currency import currency_sync_with_exchangeratesapi


User = get_user_model()


class UserViewSetTests(APITestCase):
    def setUp(self) -> None:
        currency_sync_with_exchangeratesapi()
        self.base_currency = CurrencyRate.objects.get(currency=settings.BASE_CURRENCY)
        self.user = SuperUserFactory(currency_id=self.base_currency.id)
        token, token_create = Token.objects.get_or_create(user_id=self.user.id)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_list(self) -> None:
        url = reverse('users:users-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()['result'])
        self.assertEqual(len(response.json()['data']), User.objects.count())

    def test_retrieve(self) -> None:
        url = reverse('users:users-detail', kwargs={'pk': self.user.id})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()['result'])
        data = response.json()['data']
        self.assertEqual(data['id'], self.user.id)
        self.assertEqual(data['email'], self.user.email)
        self.assertEqual(data['name'], self.user.name)
        self.assertEqual(data['currency_id'], self.base_currency.id)

    def test_create(self) -> None:
        url = reverse('users:users-list')
        create_data = {'email': 'user-1@exchanged.com', 'name': 'Star Wars', 'balance': 8888, 'password': '654321qw', 'password2': '654321qw', 'currency_id': self.base_currency.id}
        response = self.client.post(url, data=create_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(response.json()['result'])
        data = response.json()['data']
        self.assertIn('id', data)
        self.assertEqual(data['email'], data['email'])
        self.assertEqual(data['name'], data['name'])
        self.assertEqual(data['currency_id'], data['currency_id'])

    def test_update(self) -> None:
        url = reverse('users:users-detail', kwargs={'pk': self.user.id})
        update_data = {'email': 'user-1@exchanged.com', 'name': 'Star Wars', 'balance': 8888, 'password': '654321q3', 'password2': '654321q3'}
        response = self.client.patch(url, data=update_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()['result'])
        response_data = response.json()['data']
        self.assertEqual(response_data['email'], update_data['email'])
        self.assertEqual(response_data['name'], update_data['name'])
        self.assertEqual(response_data['currency_id'], self.base_currency.id)
        self.assertTrue(User.objects.get(id=self.user.id).check_password(update_data['password2']))

    def test_delete(self) -> None:
        url = reverse('users:users-detail', kwargs={'pk': self.user.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()['result'])
        self.assertEqual(response.json()['data'], {})
        self.assertFalse(User.objects.get(id=self.user.id).is_active)
