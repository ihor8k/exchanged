from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from apps.core.fields import IdPrimaryKeyRelatedField
from apps.core.models import CurrencyRate
from apps.users.models import UserBalanceHistory


User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    currency_id = IdPrimaryKeyRelatedField(label=_('Currency ID'), read_only=True)

    class Meta:
        model = User
        fields = ('id', 'balance', 'email', 'currency_id', 'date_modified', 'date_created', 'name', 'is_active', 'is_superuser',)


class CreateUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(min_length=8, max_length=64, required=True)
    password2 = serializers.CharField(min_length=8, max_length=64, required=True)
    currency_id = IdPrimaryKeyRelatedField(label=_('Currency ID'), queryset=CurrencyRate.active.all())

    class Meta:
        model = User
        fields = ('balance', 'currency_id', 'email', 'name', 'password', 'password2',)

    def validate_password2(self, value):
        password1 = self.initial_data.get('password', '')
        if password1 != value:
            raise serializers.ValidationError(_('Passwords do not match!'))
        return value

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError(_('This E-mail is already in use!'))
        return value


class UpdateUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=False, min_length=8, max_length=64)
    password2 = serializers.CharField(required=False, min_length=8, max_length=64)

    class Meta:
        model = User
        fields = ('name', 'email', 'is_active', 'password', 'password2',)
        extra_kwargs = {'name': {'required': False}, 'email': {'required': False}, 'is_active': {'read_only': True}}

    def validate(self, attrs):
        is_password = 'password' in self.initial_data
        is_password2 = 'password2' in self.initial_data
        if is_password and not is_password2:
            raise serializers.ValidationError({'password2': _('Passwords do not match!')})
        return attrs

    def validate_password2(self, value):
        is_password = 'password' in self.initial_data
        if not is_password:
            raise serializers.ValidationError(_('Passwords do not match!'))

        password1 = self.initial_data['password']
        if password1 != value:
            raise serializers.ValidationError(_('Passwords do not match!'))

        return value

    def validate_email(self, value):
        user = User.objects.filter(email=value).first()
        if user and user.id != self.instance.id:
            raise serializers.ValidationError(_('This E-mail is already in use!'))
        return value


class UserBalanceHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserBalanceHistory
        fields = ('amount', 'date_created', 'type_transaction',)
