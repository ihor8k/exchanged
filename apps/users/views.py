from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from apps.core.services.drf import Pagination, base_response, empty_base_response_scheme, errors_response_scheme, BaseResponseAutoSchema
from apps.users.models import UserBalanceHistory
from apps.users.permissions import HasModelPermissions
from apps.users import services
from apps.users import serializers


User = get_user_model()


class UserViewSet(ViewSet):
    permission_classes = (HasModelPermissions,)
    permission_model = User
    pagination_class = Pagination
    queryset = User.objects.all().order_by('-date_created')

    @swagger_auto_schema(responses={status.HTTP_200_OK: serializers.UserSerializer(many=True),
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_403_FORBIDDEN: errors_response_scheme(_('Forbidden')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Get Users'),
                         operation_description=_('Returns a list of all users.'))
    def list(self, request: Request) -> Response:
        paginator = self.pagination_class()
        objects = paginator.paginate_queryset(self.queryset, request)
        serializer = serializers.UserSerializer(objects, many=True)
        return paginator.get_paginated_response(serializer.data)

    @swagger_auto_schema(responses={status.HTTP_200_OK: serializers.UserSerializer,
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_403_FORBIDDEN: errors_response_scheme(_('Forbidden')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Get User'),
                         operation_description=_('Returns specified user.'))
    def retrieve(self, request: Request, pk: int) -> Response:
        instance = get_object_or_404(self.queryset, id=pk)
        serializer = serializers.UserSerializer(instance)
        return base_response(serializer.data)

    @swagger_auto_schema(request_body=serializers.CreateUserSerializer,
                         responses={status.HTTP_200_OK: serializers.UserSerializer,
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_403_FORBIDDEN: errors_response_scheme(_('Forbidden')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Create User'),
                         operation_description=_('Returns specified user.'))
    def create(self, request: Request) -> Response:
        serializer = serializers.CreateUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = services.create_user(serializer.validated_data)
        instance_serializer = serializers.UserSerializer(instance, context={'request': request})
        return base_response(instance_serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(request_body=serializers.UpdateUserSerializer,
                         responses={status.HTTP_200_OK: serializers.UserSerializer,
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_403_FORBIDDEN: errors_response_scheme(_('Forbidden')),
                                    status.HTTP_404_NOT_FOUND: errors_response_scheme(_('Not Found')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Update User'),
                         operation_description=_('Returns specified user.'))
    def partial_update(self, request: Request, pk: int) -> Response:
        instance = get_object_or_404(self.queryset, id=pk)
        serializer = serializers.UpdateUserSerializer(instance=instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        updated_instance = services.update_user(instance, serializer.validated_data)
        updated_serializer = serializers.UserSerializer(updated_instance, context={'request': request})
        return base_response(updated_serializer.data)

    @swagger_auto_schema(responses={status.HTTP_200_OK: empty_base_response_scheme(),
                                    status.HTTP_403_FORBIDDEN: errors_response_scheme(_('Forbidden')),
                                    status.HTTP_404_NOT_FOUND: errors_response_scheme(_('Not Found')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Delete User'),
                         operation_description=_('Returns empty data.'))
    def destroy(self, request: Request, pk: int) -> Response:
        instance = get_object_or_404(self.queryset, id=pk)
        instance.delete()
        return base_response()


class UserBalanceHistoryViewSet(ViewSet):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(responses={status.HTTP_200_OK: serializers.UserBalanceHistorySerializer(many=True),
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Get My Balance History'),
                         operation_description=_('Returns a list of all balance histories.'))
    def list(self, request: Request) -> Response:
        queryset = UserBalanceHistory.objects.filter(user_id=request.user.id).order_by('-date_created')
        serializer = serializers.UserBalanceHistorySerializer(queryset, many=True)
        return base_response(serializer.data)
