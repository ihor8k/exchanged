from typing import Dict
from django.conf import settings
from django.contrib.auth import get_user_model
from apps.users.serializers import CreateUserSerializer, UpdateUserSerializer


User = get_user_model()


def create_user(data: Dict) -> settings.AUTH_USER_MODEL:
    instance = User()

    for field in CreateUserSerializer.Meta.fields:
        if field != 'password' and hasattr(instance, field):
            setattr(instance, field, data[field])

    instance.set_password(data['password2'])
    instance.save()

    return instance


def update_user(instance: settings.AUTH_USER_MODEL, data: Dict) -> settings.AUTH_USER_MODEL:
    update_fields = []

    for field in UpdateUserSerializer.Meta.fields:
        if hasattr(instance, field) and field in data:
            setattr(instance, field, data[field])
            update_fields.append(field)

    if data.get('password2'):
        instance.set_password(data['password2'])
        update_fields.append('password')

    instance.save(update_fields=update_fields)

    return instance
