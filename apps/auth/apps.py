from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AuthConfig(AppConfig):
    name = 'apps.auth'
    label = 'exchangedauth'
    verbose_name = _('Authentication and Authorization')
