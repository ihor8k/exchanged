from typing import Dict
from django.contrib.auth import authenticate as django_authenticate
from django.utils.translation import gettext_lazy as _
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError


def authenticate(credentials: Dict) -> Dict:
    if all(credentials.values()):
        user = django_authenticate(**credentials)
        if user:
            if not user.is_active:
                raise ValidationError(_('User account is disabled.'))

            token, created = Token.objects.get_or_create(user=user)

            return {'token': token, 'user': user}
        else:
            raise ValidationError(_('Unable to log in with provided credentials.'))
    else:
        raise ValidationError(_("Must include 'email' and 'password'."))
