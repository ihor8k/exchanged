from django.urls import path
from apps.auth import views


app_name = 'auth'


urlpatterns = [
    path('login', views.LogInAPIView.as_view(), name='login'),
]
