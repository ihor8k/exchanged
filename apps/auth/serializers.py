from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from apps.core.fields import IdPrimaryKeyRelatedField


User = get_user_model()


class LogInSerializer(serializers.Serializer):
    email = serializers.EmailField(label=_('Email'))
    password = serializers.CharField(label=_('Password'), max_length=128)

    class Meta:
        fields = ('email', 'password')


class LogInUserResponseSerializer(serializers.ModelSerializer):
    currency_id = IdPrimaryKeyRelatedField(label=_('Currency ID'), read_only=True)
    group_ids = IdPrimaryKeyRelatedField(label=_('Group IDs'), many=True, read_only=True, source='groups')
    date_created = serializers.DateTimeField(label=_('Data created'), read_only=True)
    date_modified = serializers.DateTimeField(label=_('Data modified'), read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'balance', 'currency_id', 'date_created', 'date_modified', 'group_ids')


class LogInResponseSerializer(serializers.Serializer):
    token = serializers.CharField(label=_('Token'))
    user = LogInUserResponseSerializer(label=_('User info'))

    class Meta:
        fields = ('token', 'user')
