from django.utils.translation import gettext_lazy as _
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.auth import serializers
from apps.auth.services import authenticate
from apps.core.services.drf import base_response, errors_response_scheme, BaseResponseAutoSchema


class LogInAPIView(APIView):
    permission_classes = ()
    authentication_classes = ()

    @swagger_auto_schema(request_body=serializers.LogInSerializer,
                         responses={status.HTTP_200_OK: serializers.LogInResponseSerializer,
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('User Login'),
                         operation_description=_('Returns specified user and token.'))
    def post(self, request: Request) -> Response:
        serializer = serializers.LogInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        credentials = authenticate(serializer.data)
        response_serializer = serializers.LogInResponseSerializer(credentials)
        return base_response(response_serializer.data)
