# Generated by Django 2.0 on 2020-03-03 16:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CurrencyRate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Data created')),
                ('date_modified', models.DateTimeField(auto_now=True, verbose_name='Data modified')),
                ('is_active', models.BooleanField(default=True, verbose_name='Is active')),
                ('currency', models.CharField(max_length=12, verbose_name='Currency')),
                ('rate', models.DecimalField(blank=True, decimal_places=10, max_digits=19, verbose_name='Rate')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
