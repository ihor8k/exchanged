from enum import Enum


class TypesTransaction(Enum):
    add = '+'
    sub = '-'
