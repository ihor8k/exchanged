from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.core.managers import ActiveManager


class BaseModel(models.Model):
    date_created = models.DateTimeField(_('Data created'), auto_now_add=True)
    date_modified = models.DateTimeField(_('Data modified'), auto_now=True)
    is_active = models.BooleanField(_('Is active'), blank=True, default=True)

    objects = models.Manager()
    active = ActiveManager()

    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):
        self.is_active = False
        self.save(update_fields=['is_active'])
        return self


class CurrencyRate(BaseModel):
    currency = models.CharField(_('Currency'), max_length=12)
    rate = models.DecimalField(_('Rate'), max_digits=19, decimal_places=10, blank=True)
