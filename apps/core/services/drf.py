from collections import OrderedDict
from typing import Dict, Union, List, Any
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from drf_yasg import openapi
from drf_yasg.inspectors import SwaggerAutoSchema
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import exception_handler


class Pagination(PageNumberPagination):
    page_size_query_param = '_page_size'
    page_query_param = '_page'
    page_size = 10
    max_page_size = 100

    def get_paginated_response(self, data: Dict, **kwargs) -> Response:
        headers = kwargs.pop('headers', {})
        headers.update(**{'x-total-count': self.page.paginator.count})
        return base_response(data, headers=headers)


def _generate_errors_from_list(data: List, **kwargs) -> List:
    errors = []
    source = kwargs.get('source')
    for value in data:
        if isinstance(value, str):
            new_error = {'detail': value, 'source': source}
            errors.append(new_error)
        elif isinstance(value, list):
            errors.extend(_generate_errors_from_list(value, **kwargs))
        elif isinstance(value, dict):
            errors.extend(_generate_errors_from_dict(value, **kwargs))
    return errors


def _generate_errors_from_dict(data: Dict, **kwargs) -> List:
    errors = []
    source = kwargs.get('source')
    for key, value in data.items():
        source_val = f'{source}.{key}' if source else key
        if isinstance(value, str):
            new_error = {'detail': value, 'source': source_val}
            errors.append(new_error)
        elif isinstance(value, list):
            kwargs['source'] = source_val
            errors.extend(_generate_errors_from_list(value, **kwargs))
        elif isinstance(value, dict):
            kwargs['source'] = source_val
            errors.extend(_generate_errors_from_dict(value, **kwargs))
    return errors


def custom_exception_handler(exc: Any, context: Any) -> Response:
    response = exception_handler(exc, context)

    if response is not None:
        errors = []
        data = response.data
        if isinstance(data, dict):
            errors.extend(_generate_errors_from_dict(data))
        elif isinstance(data, list):
            errors.extend(_generate_errors_from_list(data))
        response.data = {'errors': errors}
    else:
        if settings.IS_PROD:
            # TODO sentry
            pass
        else:
            raise exc
        exc = APIException(exc)
        response = exception_handler(exc, context)
        response.data = {'errors': [{'detail': _('Internal Server Error'), 'source': None}]}

    response.data['result'] = False

    return response


def base_response(data: Union[Dict, List, None] = None, result: bool = True, **kwargs) -> Response:
    return Response({'result': result, 'data': data or {}}, **kwargs)


def empty_base_response_scheme(description: str = '') -> openapi.Response:
    description = description or _('An empty object returns.')
    return openapi.Response(description, openapi.Schema(type=openapi.TYPE_OBJECT, description=_('An empty object')))


def errors_response_scheme(description: str) -> openapi.Response:
    return openapi.Response(description, openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties=OrderedDict((
            ('errors', openapi.Schema(
                type=openapi.TYPE_ARRAY,
                items=openapi.Schema(type=openapi.TYPE_OBJECT, properties=OrderedDict(
                    (('detail', openapi.Schema(type=openapi.TYPE_STRING, description=_('Detail message.'))),
                     ('source', openapi.Schema(type=openapi.TYPE_STRING, description=_(f'Source field. {settings.SERIALIZER_FIELD_NULLABLE_HELP_TEXT}'))),)
                ), required=['detail', 'source']),
                description=_('Error Information')
            )),
            ('result', openapi.Schema(type=openapi.TYPE_BOOLEAN, default=False, description=_('Result'))),
        )),
        required=['result', 'errors']
    ))


class BaseResponseAutoSchema(SwaggerAutoSchema):
    def get_responses(self) -> openapi.Responses:
        responses = super().get_responses()

        if not responses.keys():
            return responses

        status_code = list(responses.keys())[0]
        if status.HTTP_200_OK <= int(status_code) < status.HTTP_300_MULTIPLE_CHOICES:
            responses[status_code]['schema'] = {'allOf': [
                openapi.Schema(type=openapi.TYPE_OBJECT, properties={'data': responses[status_code]['schema']}, description=_('Data Information')),
                openapi.Schema(type=openapi.TYPE_OBJECT, properties={'result': openapi.Schema(type=openapi.TYPE_BOOLEAN, default=True, description=_('Result'))})
            ]}
        else:
            responses[status_code]['schema'] = {'allOf': [
                openapi.Schema(type=openapi.TYPE_OBJECT, properties={'errors': responses[status_code]['schema']}, description=_('Error Information')),
                openapi.Schema(type=openapi.TYPE_OBJECT, properties={'result': openapi.Schema(type=openapi.TYPE_BOOLEAN, default=False, description=_('Result'))})
            ]}

        return responses
