import requests

from decimal import Decimal
from typing import Union
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import F
from apps.core.models import CurrencyRate
from apps.users.models import UserBalanceHistory
from apps.core.types import TypesTransaction


User = get_user_model()


def currency_sync_with_exchangeratesapi() -> None:
    url = f'https://api.exchangeratesapi.io/latest?base={settings.BASE_CURRENCY}'
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    for currency, rate in data['rates'].items():
        CurrencyRate.objects.update_or_create(currency=currency, defaults={'rate': rate})


def transfer_funds(from_user: settings.AUTH_USER_MODEL, to_user: settings.AUTH_USER_MODEL, amount: Union[Decimal, int, float]) -> None:
    if from_user.currency_id == to_user.currency_id:
        with transaction.atomic():
            User.objects.select_for_update(of=('self',)).filter(id=from_user.id).update(balance=F('balance') - amount)
            UserBalanceHistory.objects.create(amount=amount, user_id=from_user.id, type_transaction=TypesTransaction.sub.value)

            User.objects.select_for_update(of=('self',)).filter(id=to_user.id).update(balance=F('balance') + amount)
            UserBalanceHistory.objects.create(amount=amount, user_id=to_user.id, type_transaction=TypesTransaction.add.value)
    else:
        base_currency = CurrencyRate.objects.get(currency=settings.BASE_CURRENCY)
        with transaction.atomic():
            User.objects.select_for_update(of=('self',)).filter(id=from_user.id).update(balance=F('balance') - amount)
            UserBalanceHistory.objects.create(amount=amount, user_id=from_user.id, type_transaction=TypesTransaction.sub.value)

            currency_to_user = (amount * 100 * to_user.currency.rate) / (100 * base_currency.rate)
            User.objects.select_for_update(of=('self',)).filter(id=to_user.id).update(balance=F('balance') + currency_to_user)
            UserBalanceHistory.objects.create(amount=currency_to_user, user_id=to_user.id, type_transaction=TypesTransaction.add.value)
