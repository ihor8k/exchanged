from celery import shared_task
from apps.core.services.currency import currency_sync_with_exchangeratesapi as base_currency_sync_with_exchangeratesapi


@shared_task
def currency_sync_with_exchangeratesapi() -> None:
    base_currency_sync_with_exchangeratesapi()
