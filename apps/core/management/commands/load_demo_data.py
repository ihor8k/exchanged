from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils.translation import gettext_lazy as _
from apps.core.models import CurrencyRate
from apps.core.services.currency import currency_sync_with_exchangeratesapi


User = get_user_model()


class Command(BaseCommand):
    help = _('Load demo data')

    def handle(self, *args, **options):
        if settings.IS_DEV:
            currency_sync_with_exchangeratesapi()

            base_currency = CurrencyRate.objects.get(currency=settings.BASE_CURRENCY)

            User.objects.create_user(email='user-1@exchanged.com', name='Jhon Adamson', balance=80, currency_id=base_currency.id, password='12345')
            User.objects.create_user(email='user-2@exchanged.com', name='Adam Jons', balance=150.50, currency_id=CurrencyRate.objects.order_by('?').first().id, password='12345')
            User.objects.create_user(email='user-3@exchanged.com', name='Don Jons', balance=280.80, currency_id=CurrencyRate.objects.order_by('?').first().id, password='12345')
            User.objects.create_user(email='user-4@exchanged.com', name='Jon Dep', balance=480.80, currency_id=base_currency.id, password='12345')

        self.stdout.write(self.style.SUCCESS('Load demo data successfully loaded!'))
        self.stdout.flush()
