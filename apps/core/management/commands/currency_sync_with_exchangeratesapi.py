from django.core.management.base import BaseCommand
from django.utils.translation import gettext_lazy as _
from apps.core.services.currency import currency_sync_with_exchangeratesapi


class Command(BaseCommand):
    help = _('Currency sync with exchangeratesapi')

    def handle(self, *args, **options):
        currency_sync_with_exchangeratesapi()
        self.stdout.write(self.style.SUCCESS(_('Successfully loaded!')))
        self.stdout.flush()
