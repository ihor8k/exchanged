from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from apps.core.models import CurrencyRate
from apps.core import serializers
from apps.core.services.currency import transfer_funds
from apps.core.services.drf import base_response, errors_response_scheme, empty_base_response_scheme, BaseResponseAutoSchema


User = get_user_model()


class CurrencyRateViewSet(ViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = CurrencyRate.active.all().order_by('-date_created')

    @swagger_auto_schema(responses={status.HTTP_200_OK: serializers.CurrencyRateSerializer(many=True),
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Get Currencies Rate'),
                         operation_description=_('Returns a list of all currencies rate.'))
    def list(self, request: Request) -> Response:
        serializer = serializers.CurrencyRateSerializer(self.queryset, many=True)
        return base_response(serializer.data)

    @swagger_auto_schema(responses={status.HTTP_200_OK: serializers.CurrencyRateSerializer,
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_404_NOT_FOUND: errors_response_scheme(_('Not Found')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Get Currency Rate'),
                         operation_description=_('Returns specified currency rate.'))
    def retrieve(self, request: Request, pk: int) -> Response:
        instance = get_object_or_404(self.queryset, id=pk)
        serializer = serializers.CurrencyRateSerializer(instance)
        return base_response(serializer.data)


class TransferFundAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=serializers.TransferFundSerializer,
                         responses={status.HTTP_200_OK: empty_base_response_scheme(),
                                    status.HTTP_400_BAD_REQUEST: errors_response_scheme(_('Bad Request')),
                                    status.HTTP_500_INTERNAL_SERVER_ERROR: errors_response_scheme(_('Internal Server Error'))},
                         auto_schema=BaseResponseAutoSchema,
                         operation_summary=_('Transfer Fund'),
                         operation_description=_('Returns empty data.'))
    def post(self, request: Request) -> Response:
        serializer = serializers.TransferFundSerializer(data=request.data, from_user=request.user)
        serializer.is_valid(raise_exception=True)
        to_user = User.objects.get(id=serializer.validated_data['user_id'])
        amount = serializer.validated_data['amount']
        transfer_funds(request.user, to_user, amount)
        return base_response()
