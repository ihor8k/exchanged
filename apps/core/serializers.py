from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from apps.core.fields import IdPrimaryKeyRelatedField
from apps.core.models import CurrencyRate


User = get_user_model()


class CurrencyRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CurrencyRate
        fields = ('id', 'currency', 'rate', 'date_modified', 'date_created',)


class TransferFundSerializer(serializers.Serializer):
    user_id = IdPrimaryKeyRelatedField(label=_('To user ID'), queryset=User.objects.all())
    amount = serializers.DecimalField(label=_('Amount'), max_digits=19, decimal_places=10)

    class Meta:
        fields = ('user_id', 'amount',)

    def __init__(self, *args, **kwargs):
        self.from_user = kwargs.pop('from_user', None)
        super().__init__(*args, **kwargs)

    def validate_amount(self, value):
        if value <= 0:
            raise ValidationError(_('The amount cannot be less than or equal to 0!'))
        return value

    def validate_user_id(self, value):
        assert self.from_user
        if value == self.from_user.id:
            raise ValidationError(_('It is not possible to send it yourself!'))
        return value
