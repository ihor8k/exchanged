import pytest

from django.contrib.auth import get_user_model
from apps.core.models import CurrencyRate
from apps.core.services.currency import transfer_funds
from apps.core.services.drf import base_response
from apps.core.types import TypesTransaction
from apps.users.models import UserBalanceHistory


User = get_user_model()


@pytest.mark.django_db
def tests_transfer_funds_same_currency(base_currency_rate: CurrencyRate) -> None:
    from_user = User.objects.create_user(email='user-1@exchanged.com', name='Jhon Adamson', balance=80, currency_id=base_currency_rate.id, password='12345')
    to_user = User.objects.create_user(email='user-2@exchanged.com', name='Adam Jon', balance=80, currency_id=base_currency_rate.id, password='12345')

    transfer_funds(from_user, to_user, 80)
    assert User.objects.get(id=from_user.id).balance == 0
    assert User.objects.get(id=to_user.id).balance == 160

    assert UserBalanceHistory.objects.filter(user_id=from_user.id).count() == 1
    assert UserBalanceHistory.objects.get(user_id=from_user.id).type_transaction == TypesTransaction.sub.value
    assert UserBalanceHistory.objects.get(user_id=from_user.id).amount == 80

    assert UserBalanceHistory.objects.filter(user_id=to_user.id).count() == 1
    assert UserBalanceHistory.objects.get(user_id=to_user.id).type_transaction == TypesTransaction.add.value
    assert UserBalanceHistory.objects.get(user_id=to_user.id).amount == 80


@pytest.mark.django_db
def tests_transfer_funds_different_currency(base_currency_rate: CurrencyRate) -> None:
    from_user = User.objects.create_user(email='user-1@exchanged.com', name='Jhon Adamson', balance=80, currency_id=base_currency_rate.id, password='12345')
    to_user = User.objects.create_user(
        email='user-2@exchanged.com', name='Adam Jon', balance=80, currency_id=CurrencyRate.objects.exclude(id=base_currency_rate.id).order_by('?').first().id, password='12345')

    transfer_funds(from_user, to_user, 80)
    assert User.objects.get(id=from_user.id).balance == 0
    assert User.objects.get(id=to_user.id).balance == (80*100*to_user.currency.rate)/(100*base_currency_rate.rate) + 80

    assert UserBalanceHistory.objects.filter(user_id=from_user.id).count() == 1
    assert UserBalanceHistory.objects.get(user_id=from_user.id).type_transaction == TypesTransaction.sub.value
    assert UserBalanceHistory.objects.get(user_id=from_user.id).amount == 80

    assert UserBalanceHistory.objects.filter(user_id=to_user.id).count() == 1
    assert UserBalanceHistory.objects.get(user_id=to_user.id).type_transaction == TypesTransaction.add.value
    assert UserBalanceHistory.objects.get(user_id=to_user.id).amount == (80*100*to_user.currency.rate)/(100*base_currency_rate.rate)


def tests_base_response() -> None:
    response = base_response()
    assert not response.data['data']
    assert response.data['result']
