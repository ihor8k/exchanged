from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from apps.core.models import CurrencyRate
from apps.users.tests.factories import SuperUserFactory
from apps.core.services.currency import currency_sync_with_exchangeratesapi


class CurrencyRateViewSetTests(APITestCase):
    def setUp(self) -> None:
        currency_sync_with_exchangeratesapi()
        self.base_currency = CurrencyRate.objects.get(currency=settings.BASE_CURRENCY)
        user = SuperUserFactory(currency_id=self.base_currency.id)
        token, token_create = Token.objects.get_or_create(user_id=user.id)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_list(self) -> None:
        url = reverse('core:currency-rates-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()['result'])
        self.assertEqual(len(response.json()['data']), CurrencyRate.objects.count())

    def test_retrieve(self) -> None:
        url = reverse('core:currency-rates-detail', kwargs={'pk': self.base_currency.id})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.json()['result'])
        data = response.json()['data']
        self.assertEqual(data['id'], self.base_currency.id)
        self.assertEqual(data['currency'], self.base_currency.currency)
