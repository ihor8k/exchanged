from django.urls import path, include
from rest_framework.routers import SimpleRouter
from apps.core import views


app_name = 'core'

router = SimpleRouter(trailing_slash=False)
router.register('currency-rates', views.CurrencyRateViewSet, basename='currency-rates')

urlpatterns = [
    path('transfer/funds', views.TransferFundAPIView.as_view()),
    path('', include(router.urls)),
]
