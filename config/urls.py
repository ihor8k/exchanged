from django.urls import include, path
from drf_yasg.openapi import Info, Contact
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny


schema_view = get_schema_view(
   Info(
      title='Exchanged API',
      default_version='v1',
      description='Exchanged demo project',
      contact=Contact(email='ihor8k@gmail.com'),
   ),
   public=True,
   permission_classes=(AllowAny,),
)

urlpatterns = [
    path('api/', include('apps.users.urls', namespace='users')),
    path('api/', include('apps.core.urls', namespace='core')),
    path('api/', include('apps.auth.urls', namespace='auth')),
    path('api/doc/', schema_view.with_ui('redoc', cache_timeout=0), name='redoc-ui'),
]
