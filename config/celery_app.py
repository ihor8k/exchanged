import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

app = Celery('exchanged')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'currency-sync-with-exchangeratesapi': {
        'task': 'apps.core.tasks.currency_sync_with_exchangeratesapi',
        'schedule': crontab(minute='*/1'),
    },
}
