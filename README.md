## Exchanged


![](https://img.shields.io/badge/built%20for-exchanged.com-ff1544.svg)



####Need Install [docker](https://docs.docker.com/install/)


Start local:

    docker-compose up

    ----/ or /----
    docker-compose up -d postgresql
    docker-compose up -d redis
    docker-compose up app

Shell:

    # python
    docker exec -it exchanged_app /bin/sh

    # postgresql
    docker exec -it exchanged_db bash

Celery run:

    celery -A config.celery_app worker -l DEBUG
    celery -A config.celery_app beat -l DEBUG
